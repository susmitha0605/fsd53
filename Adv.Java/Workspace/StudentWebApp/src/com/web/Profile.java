package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.Student;

@WebServlet("/Profile")
public class Profile extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		session.getAttribute("emp");
		
		Student stu = (Student) session.getAttribute("stu"); 
		
		RequestDispatcher rd = request.getRequestDispatcher("StudentHome");
		rd.include(request, response);
		
		out.print("<center>");
		out.print("<table>");
		out.print("<table border='1' cellpadding='5'>");
        out.print("<tr><th colspan='2'>Profile Details</th></tr>");
        out.print("<tr><td>Student Name:</td><td>" + stu.getStudentName() + "</td></tr>");
        out.print("<tr><td>Gender:</td><td>" + stu.getGender() + "</td></tr>");
        out.print("<tr><td>Email ID:</td><td>" + stu.getEmailId() + "</td></tr>");
        out.print("<tr><td>Password:</td><td>" + stu.getPassword() + "</td></tr>");
        out.print("</table>");
        out.print("</center>");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
