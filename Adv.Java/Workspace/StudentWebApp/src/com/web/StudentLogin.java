package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.StudentDAO;
import com.model.Student;

@WebServlet("/StudentLogin")
public class StudentLogin extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");		
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
        
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.print("<html>");
		if (emailId.equalsIgnoreCase("ADMIN") && password.equalsIgnoreCase("ADMIN")) {

			RequestDispatcher rd = request.getRequestDispatcher("AdminHome");
			rd.forward(request, response);

		} else {

			StudentDAO stuDAO = new StudentDAO();
			Student stu = stuDAO.studentLogin(emailId, password);

			if (stu != null) {
				session.setAttribute("stu", stu);
				RequestDispatcher rd = request.getRequestDispatcher("StudentHome");
				rd. forward(request, response);
			} else {

				out.print("<body bgcolor=lightyellow text=red>");
				out.print("<center>");
				out.print("<h1>Invalid Credentials</h1>");

				RequestDispatcher rd = request.getRequestDispatcher("StudentLogin.html");
				rd.include(request, response);
			}
		}
		out.print("</center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}