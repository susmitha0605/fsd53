package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/AdminHome")
public class AdminHome extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		//String emailId = request.getParameter("emailId");
		
		HttpSession session = request.getSession(false);
		String emailId = (String) session.getAttribute("emailId");
		
		
		
		out.print("<body bgcolor=lightyellow text=blue>");
		out.print("<h3>Welcome " + emailId + "!</h3>");
		
		out.print("<form align='right'>");
		out.print("<a href='AdminHome'>Home</a> &nbsp; ");
		out.print("<a href='StudentLogin.html'>Logout</a>");
		out.print("</form>");
		
		out.print("<center>");
		out.print("<h1>Welcome to Admin Page </h1>");
		out.print("<h3>");
		
		out.print("<a href='GetAllStudents'>GetAllStudents</a> &nbsp; &nbsp; &nbsp;");
		
		//GetStudentById
		out.print("<a href='GetStudentById.html'>GetStudentById</a>");
		out.print("</h3>");
		out.print("</center></body>");
		
		
		out.print("</h3>");
		out.print("</center></body>");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
