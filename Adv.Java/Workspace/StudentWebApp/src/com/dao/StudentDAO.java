package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.model.Student;

public class StudentDAO {
	public Student studentLogin(String emailId, String password){
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		if(con ==null){
			System.out.println("Cannot establish connection to database...");
			return null;
		}
		
		
		
		try {
			String selectQry = "select * from student where emailId = ? and password = ?";
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if(rs.next()){
				Student stu = new Student();
				stu.setStudentId(rs.getInt("studentId"));
				stu.setStudentName(rs.getString(2));
				stu.setEmailId(rs.getString(3));
				stu.setGender(rs.getString(4));
				stu.setPassword(rs.getString(5));
				
				return stu;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally{
			if(con != null){
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		return null;
	}
	
	public List<Student> getAllStudents(){
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		if(con == null){
			System.out.println("Cannot Establish Connection To The Database...");
		}
		
		
		try {
			String selectQry = "select * from student";
			pst = con.prepareStatement(selectQry);
			rs = pst.executeQuery();
			
			List<Student> stuList = new ArrayList<Student>();
			while(rs.next()){
				Student stu = new Student();
				
				stu.setStudentId(rs.getInt("studentId"));
				stu.setStudentName(rs.getString(2));
				stu.setEmailId(rs.getString(3));
				stu.setGender(rs.getString(4));
				stu.setPassword(rs.getString(5));
				
				stuList.add(stu);
			}
			return stuList;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(con!=null){
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
		return null;
				
	}
	

	
	
public int studentRegister(Student stu) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Cannot Establish the Connection to the Database...");
			return 0;
		}
		
		try {
			String qry = "insert into student(studentName, gender, emailId, password) values (?, ?, ?, ?, ?)";
			
			pst = con.prepareStatement(qry);
			
			pst.setString(1, stu.getStudentName());
			pst.setString(2, stu.getGender());
			pst.setString(3, stu.getEmailId());
			pst.setString(4, stu.getPassword());
			
			int result = pst.executeUpdate();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

public Student getStudentById(int studentId) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	if (con == null) {
		System.out.println("Cannot Establish the Connection to the Database...");
		return null;
	}
			
	try {
		String qry = "select * from student where studentId = ?";
		
		pst = con.prepareStatement(qry);
		pst.setInt(1, studentId);
		rs = pst.executeQuery();
		
		if (rs.next()) {
			
			Student stu = new Student();
			
			stu.setStudentId(rs.getInt(1));
			stu.setStudentName(rs.getString(2));
			stu.setGender(rs.getString(3));
			stu.setEmailId(rs.getString(4));
			stu.setPassword(rs.getString(5));
			
			return stu;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
			
	return null;		
}
}

